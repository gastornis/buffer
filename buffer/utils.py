import logging


def get_logger(name: str) -> logging.Logger:
    package_removed = ".".join(name.split(".")[1:])
    return logging.getLogger(package_removed)
